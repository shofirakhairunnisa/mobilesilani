part of 'widgets.dart';

Container button(String text, core, page) {
  return Container(
    width: core,
    margin: EdgeInsets.symmetric(vertical: 20),
    height: 50,
    padding: EdgeInsets.symmetric(horizontal: defaultMargin),
    child: ElevatedButton(
      onPressed: () {
        Get.to(page);
      },
      style: ElevatedButton.styleFrom(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8)),
          primary: mainColor),
      child: Text(
        text,
        style: GoogleFonts.poppins(
            color: Colors.white,
            fontWeight: FontWeight.w600,
            fontSize: 16),
      ),
    ),
  );
}

Container buttonGrey(String text, core, page) {
  return Container(
    width: core,
    margin: EdgeInsets.symmetric(vertical: 20),
    height: 50,
    padding: EdgeInsets.symmetric(horizontal: defaultMargin),
    child: ElevatedButton(
      onPressed: () {
        Get.to(page);
      },
      style: ElevatedButton.styleFrom(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8)),
          primary: greyColor),
      child: Text(
        text,
        style: GoogleFonts.poppins(
            color: Colors.white,
            fontWeight: FontWeight.w600,
            fontSize: 16),
      ),
    ),
  );
}

Container buttonOutlineBlue(String text, page) {
  return Container(
    width: double.infinity,
    margin: EdgeInsets.symmetric(vertical: 26),
    height: 50,
    padding: EdgeInsets.symmetric(horizontal: defaultMargin),
    child: OutlinedButton(
      onPressed: () {
        Get.to(page);
      },
      style: OutlinedButton.styleFrom(
          side: BorderSide(width: 2, color: secondaryColor),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8))),
      child: Text(
        text,
        style: GoogleFonts.poppins(
            color: secondaryColor,
            fontWeight: FontWeight.w600,
            fontSize: 16),
      ),
    ),
  );
}