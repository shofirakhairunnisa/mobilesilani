import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:silanimobile/shared/shared.dart';
import 'package:silanimobile/ui/pages/pages.dart';
import 'package:get/get.dart';

part 'custom_bottom_navbar.dart';
part 'field_form.dart';
part 'custom_button.dart';
part 'custom_list.dart';