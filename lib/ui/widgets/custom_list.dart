part of 'widgets.dart';

InkWell listLayanan(String provider, String place, String status) {
  return InkWell(
    child: Container(
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(width: 1, color: Colors.black12))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(provider,
                  style: TextStyle(
                      color: secondaryColor, fontWeight: FontWeight.w600)),
              Text(place,
                  style: TextStyle(color: greyColor2, fontSize: 12, height: 2)),
              Text(status,
                  style: TextStyle(
                      color: mainColor,
                      fontWeight: FontWeight.w600,
                      fontSize: 12,
                      height: 2)),
            ],
          ),
          Icon(
            Icons.chevron_right,
            size: 35,
            color: greyColor2,
          )
        ],
      ),
    ),
    onTap: () {
      Get.to(DetailLayanan());
    },
  );
}

InkWell listDetail(String date, String provider, bool status) {
  return InkWell(
    child: Container(
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(width: 1, color: Colors.black12))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(date,
                  style: TextStyle(color: greyColor2, fontSize: 12)),
              Text(provider,
                  style: TextStyle(
                      color: secondaryColor, fontWeight: FontWeight.w600, height: 2)),
              Container(
                margin: EdgeInsets.only(top: 15),
                padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: status != false ? secondaryColor : greyColor2),
                child: status != false ? Text(
                  'On Progress',
                  style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: 12),
                ) : Text(
                  'Selesai',
                  style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600, fontSize: 12),
                ),
              )
            ],
          ),
          Icon(
            Icons.chevron_right,
            size: 35,
            color: greyColor2,
          )
        ],
      ),
    ),
    onTap: () {
      Get.to(DetailLayanan());
    },
  );
}
