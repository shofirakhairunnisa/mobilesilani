part of 'widgets.dart';

Container inputField(
    TextEditingController controller, bool obscure, String label) {
  return Container(
      width: double.infinity,
      margin:
          const EdgeInsets.symmetric(horizontal: defaultMargin, vertical: 15),
      child: TextFormField(
        controller: controller,
        obscureText: obscure,
        decoration: InputDecoration(
          labelText: label,
          labelStyle: TextStyle(color: Colors.black45),
          fillColor: Colors.white,
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: mainColor, width: 2),
            borderRadius: BorderRadius.circular(8),
          ),
        ),
      ));
}
