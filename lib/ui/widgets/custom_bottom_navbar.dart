part of 'widgets.dart';

class CustomBottomNavbar extends StatelessWidget {
  const CustomBottomNavbar({Key? key, this.selectedIndex = 0, this.onTap})
      : super(key: key);

  final int selectedIndex;
  final Function(int index)? onTap;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      width: double.infinity,
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          GestureDetector(
            onTap: () {
              if (onTap != null) {
                onTap!(0);
              }
            },
            child: Column(children: [
              Icon(Icons.home,
                  color: (selectedIndex == 0) ? secondaryColor : greyColor,
                  size: 28),
              Text('Home', style: TextStyle(color: (selectedIndex == 0) ? secondaryColor : greyColor),)
            ]),
          ),
          GestureDetector(
            onTap: () {
              if (onTap != null) {
                onTap!(1);
              }
            },
            child: Column(children: [
              Icon(Icons.web,
                  color: (selectedIndex == 1) ? secondaryColor : greyColor,
                  size: 28),
              Text('Tips', style: TextStyle(color: (selectedIndex == 1) ? secondaryColor : greyColor, fontSize: 14),)
            ]),
          ),
          GestureDetector(
            onTap: () {
              if (onTap != null) {
                onTap!(2);
              }
            },
            child: Column(children: [
              Icon(Icons.next_week,
                  color: (selectedIndex == 2) ? secondaryColor : greyColor,
                  size: 28),
              Text('Layanan', style: TextStyle(color: (selectedIndex == 2) ? secondaryColor : greyColor),)
            ]),
          ),
          GestureDetector(
            onTap: () {
              if (onTap != null) {
                onTap!(3);
              }
            },
            child: Column(children: [
              Icon(Icons.settings,
                  color: (selectedIndex == 3) ? secondaryColor : greyColor,
                  size: 28),
              Text('Pengaturan', style: TextStyle(color: (selectedIndex == 3) ? secondaryColor : greyColor),)
            ]),
          )
        ],
      ),
    );
  }
}
