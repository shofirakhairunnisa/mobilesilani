part of 'pages.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return GeneralPageCustom(
        title: 'Rusty Shackleford',
        alamat: 'Dusun Shackleford',
        date: '10 Juni 1951',
        nik: 'ID-1234-5678-91234556',
        directButton: () {
          Get.to(DetailProfile());
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              child: Text('Hubungi Petugas',
                  style: TextStyle(
                      fontSize: 20, fontWeight: FontWeight.w600, height: 5)),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              child: Text(
                  'Kami siap membantu layanan berikut :\n1. Transportasi ke fasilitas kesehatan\n2. Perawatan di rumah\n3. Keadaan Darurat',
                  style: TextStyle(fontSize: 14, height: 2)),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  cardContact('whatsapp.png', '5DCE6A', 'WhatsApp'),
                  cardContact('telepon.png', '2F80ED', 'Hubungi Kami'),
                ],
              ),
            )
          ],
        ));
  }

  InkWell cardContact(String image, String color, String text) {
    return InkWell(
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 15),
        padding: EdgeInsets.only(top: 15),
        height: 100,
        width: 130,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: color.toColor(),
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image(image: AssetImage(image), height: 32, width: 32),
              Text(
                text,
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.white,
                    height: 3),
              )
            ],
          ),
        ),
      ),
      onTap: () {},
    );
  }
}
