part of '../pages.dart';

class LayananHomeCare extends StatefulWidget {
  @override
  _LayananHomeCareState createState() => _LayananHomeCareState();
}

class _LayananHomeCareState extends State<LayananHomeCare> {
  @override
  Widget build(BuildContext context) {
    return GeneralPage(
        title: 'Fasilitas Perawatan',
        onBackButtonPressed: () {
          Navigator.pop(context);
        },
        child: Column(
          children: [
            Container(
              width: double.infinity,
              color: Colors.black12,
              padding: EdgeInsets.all(20),
              child: Text(
                'Pilih Fasilitas Perawatan',
                style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
              ),
            ),
            listLayanan('Insan Medika Persada Homecare', 'Persada',
                'Ditanggung oleh KIS Anda'),
            listLayanan('Jasa Perawatan Homecare Jogja', 'Jln Benyo Pajangan', ''),
          ],
        ));
  }
}
