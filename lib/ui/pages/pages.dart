import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:silanimobile/ui/widgets/widgets.dart';
import 'package:supercharged/supercharged.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:silanimobile/shared/shared.dart';

part 'general_page.dart';
part 'general_page_custom.dart';
part 'main_page.dart';
part 'sign_in_page.dart';
part 'sign_up_page.dart';
part 'home_page.dart';
part 'tips_page.dart';

// profile
part 'profile.dart';
part 'profile/tentang_apps.dart';
part 'profile/update_pass.dart';
part 'profile/update_profile.dart';
part 'profile/detail_profile.dart';

//layanan
part 'layanan_page.dart';
part 'bansos/register_bansos_page.dart';
part 'bansos/konfirmasi_register_bansos.dart';
part 'kesehatan/layanan_kesehatan.dart';
part 'kesehatan/detail_layanan_kesehatan.dart';
part 'homecare/layanan_homecare.dart';