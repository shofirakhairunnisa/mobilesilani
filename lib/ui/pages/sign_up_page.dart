part of 'pages.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  TextEditingController nikController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController tempatLahirController = TextEditingController();
  TextEditingController tanggalLahirController = TextEditingController();
  TextEditingController alamatController = TextEditingController();
  TextEditingController noTeleponController = TextEditingController();
  TextEditingController noBpjsController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return GeneralPage(
      title: 'Pendaftaran',
      onBackButtonPressed: () {
        Navigator.pop(context);
      },
      child: Column(
        children: [
          Row(
            children: [
              Container(
                width: 100,
                height: 100,
                padding: EdgeInsets.all(5),
                decoration: BoxDecoration(
                    image: DecorationImage(image: AssetImage('point1.png'))),
              ),
              Text('Lengkapi data dirimu \ndi bawah ini ya',
                  style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 22,
                      color: secondaryColor)),
            ],
          ),
          inputField(nikController, false, 'NIK'),
          inputField(nameController, false, 'Nama Lengkap'),
          inputField(tempatLahirController, false, 'Tempat Lahir'),
          inputField(tanggalLahirController, false, 'Tanggal Lahir'),
          inputField(alamatController, false, 'Alamat'),
          inputField(noTeleponController, false, 'No Telepon'),
          inputField(noBpjsController, false, 'No Bpjs'),
          inputField(emailController, false, 'Email'),
          inputField(passwordController, true, 'Password'),
          Container(
            width: double.infinity,
            margin: EdgeInsets.symmetric(vertical: 26),
            height: 50,
            padding: EdgeInsets.symmetric(horizontal: defaultMargin),
            child: ElevatedButton(
              onPressed: () {
                signUp();
              },
              style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8)),
                  primary: mainColor),
              child: Text(
                'Sign Up',
                style: GoogleFonts.poppins(
                    color: Colors.white,
                    fontWeight: FontWeight.w600,
                    fontSize: 16),
              ),
            ),
          ),
        ],
      ),
    );
  }

  //  FUNCTION TO CALL SignUp POST
  Future<void> signUp() async {
    if (passwordController.text.isNotEmpty &&
        emailController.text.isNotEmpty &&
        nameController.text.isNotEmpty) {
      var response = await http.post(
          Uri.parse(
              "http://117.53.44.201:8999/api/v1/mobile/auth/register/member"),
          body: ({
            'name': nameController.text,
            'email': emailController.text,
            'password': passwordController.text,
          }));
      if (response.statusCode == 200) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text('Create Account $response.body.name Success!'),
        ));
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) {
          return SignInPage();
        }));
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text('Internal Server Error'),
        ));
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Blank Field Not Allowed'),
      ));
    }
  }
}
