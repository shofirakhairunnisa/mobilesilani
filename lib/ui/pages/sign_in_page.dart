part of 'pages.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({Key? key}) : super(key: key);

  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  bool isLoading = false;

  //Function
  signIn(String email, String pass) async {
    String url = "http://117.53.44.201:8999/api/v1/mobile/auth/token";
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    Map body = {"login": email, "pwd": pass};
    var jsonResponse;
    var res = await http.post(Uri.parse(url), body: body);

    //  Need check Api
    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);

      print("Response status: ${res.statusCode}");
      print("Response status: ${res.body}");

      if (jsonResponse != null) {
        setState(() {
          isLoading = false;
        });

        sharedPreferences.setString("token", jsonResponse['token']);
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (BuildContext context) => MainPage()),
            (Route<dynamic> route) => false);
      }
    } else {
      setState(() {
        isLoading = false;
      });

      print("Response status: ${res.body}");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(children: [
      Column(
        children: [
          Container(
            width: 300,
            height: 150,
            margin: EdgeInsets.only(top: 15),
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('login-bg.png'), fit: BoxFit.fill)),
            child: Container(
              decoration: BoxDecoration(
                  image: DecorationImage(image: AssetImage('logo.png'))),
            ),
          ),
          Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Silani',
                  style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 32,
                      color: secondaryColor),
                ),
                Text(
                  'Sistem Informasi \nLanjut Usia',
                  style: TextStyle(
                      fontWeight: FontWeight.w500,
                      color: greyColor,
                      fontSize: 16,
                      letterSpacing: 2),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          ),
          inputField(emailController, false, 'Username/Email'),
          inputField(passwordController, true, 'Password'),
          Container(
            width: double.infinity,
            margin: const EdgeInsets.only(top: 26),
            height: 50,
            padding: const EdgeInsets.symmetric(horizontal: defaultMargin),
            child: ElevatedButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => MainPage()));
                // login();
              },
              style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8)),
                  primary: mainColor),
              child: Text(
                'Masuk',
                style: GoogleFonts.poppins(
                    color: Colors.white,
                    fontWeight: FontWeight.w600,
                    fontSize: 16),
              ),
            ),
          ),
          Container(
            width: double.infinity,
            margin: EdgeInsets.symmetric(vertical: 26),
            height: 50,
            padding: EdgeInsets.symmetric(horizontal: defaultMargin),
            child: OutlinedButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => SignUpPage()));
              },
              style: OutlinedButton.styleFrom(
                  side: BorderSide(width: 2, color: mainColor),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8))),
              child: Text(
                'Daftar',
                style: GoogleFonts.poppins(
                    color: mainColor,
                    fontWeight: FontWeight.w600,
                    fontSize: 16),
              ),
            ),
          )
        ],
      ),
    ]));
  }

  //  FUNCTION TO CALL LOGIN POST
  Future<void> login() async {
    if (passwordController.text.isNotEmpty && emailController.text.isNotEmpty) {
      var response = await http.post(
          Uri.parse("http://117.53.44.201:8999/api/v1/mobile/auth/token"),
          body: ({
            'login': emailController.text,
            'pwd': passwordController.text,
          }));
      print(response.body);
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      if (response.statusCode == 200) {
        // Get.to(MainPage());
        print(emailController.text);
        print(passwordController.text);
        var jsonResponse = json.decode(response.body);

        // if (jsonResponse != null) {
        //   setState(() {
        //     isLoading = false;
        //   });

            sharedPreferences.setString("token", jsonResponse['data']['token']);
            print(sharedPreferences.setString("token", jsonResponse['data']['token']));
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (BuildContext context) => MainPage()),
                    (Route<dynamic> route) => false);
        //   } else {
        //     ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        //                   content: Text('JsonResponse is Null'),
        //   ));
        // }
        // Navigator.push(context, MaterialPageRoute(builder: (context) => MainPage()));
      } else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text('Incorrect password or username! Please try again'),
        ));
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Blank Field Not Allowed'),
      ));
    }
  }
}
