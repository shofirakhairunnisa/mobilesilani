part of 'pages.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return GeneralPageCustom(
        title: 'Rusty Shackleford',
        alamat: 'Dusun Shackleford',
        date: '10 Juni 1951',
        nik: 'ID-1234-5678-91234556',
        onBackButtonPressed: () {
          Navigator.pop(context);
        },
        child: Column(
          children: [
            listBar('Ubah Profil', UpdateProfile()),
            listBar('Ganti Password', UpdatePassword()),
            listBar('Tentang Aplikasi', TentangApps()),
            InkWell(
              child: Container(
                padding: EdgeInsets.all(18),
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(width: 1, color: Colors.black12))),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Log Out',
                      style:
                          TextStyle(fontWeight: FontWeight.w500, fontSize: 14),
                    ),
                    Icon(Icons.chevron_right)
                  ],
                ),
              ),
              onTap: () {},
            ),
          ],
        ));
  }

  InkWell listBar(String text, page) {
    return InkWell(
            child: Container(
              padding: EdgeInsets.all(18),
              decoration: BoxDecoration(
                  border: Border(
                      bottom: BorderSide(width: 1, color: Colors.black12))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    text,
                    style:
                        TextStyle(fontWeight: FontWeight.w500, fontSize: 14),
                  ),
                  Icon(Icons.chevron_right)
                ],
              ),
            ),
            onTap: () {
              Get.to(page);
            },
          );
  }
}
