part of '../pages.dart';

class RegisterBansos extends StatefulWidget {
  @override
  _RegisterBansosState createState() => _RegisterBansosState();
}

class _RegisterBansosState extends State<RegisterBansos> {
  @override
  Widget build(BuildContext context) {
    return GeneralPage(
        title: 'Konfirmasi',
        onBackButtonPressed: () {
          Navigator.pop(context);
        },
        child: Column(
          children: [
            Container(
              width: 200,
              height: 160,
              margin: EdgeInsets.only(top: 15),
              padding: EdgeInsets.all(5),
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('img-daftar-bansos.png'),
                      fit: BoxFit.fill)),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 25, vertical: 20),
              child: Text(
                  'Untuk menjadi penerima bantuan sosial, ada beberapa persyaratan yang dibutuhkan. Kami akan mengirimkan seorang kader untuk mendatangi rumah Anda dan mewancarai Anda. \n\nApakah Anda bersedia?',
                  style: TextStyle(height: 2, fontWeight: FontWeight.w600),
                  textAlign: TextAlign.center),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                buttonGrey('Batal', 160, LayananPage()),
                button('Ya', 160, KonfirmasiRegisterBansos()),
              ],
            )
          ],
        ));
  }
}
