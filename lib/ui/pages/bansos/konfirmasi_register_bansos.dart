part of '../pages.dart';

class KonfirmasiRegisterBansos extends StatefulWidget {
  @override
  _KonfirmasiRegisterBansosState createState() =>
      _KonfirmasiRegisterBansosState();
}

class _KonfirmasiRegisterBansosState extends State<KonfirmasiRegisterBansos> {
  @override
  Widget build(BuildContext context) {
    return GeneralPage(
        title: 'Konfirmasi',
        onBackButtonPressed: () {
          Navigator.pop(context);
        },
        child: Column(
          children: [
            Container(
              width: 80,
              height: 80,
              margin: EdgeInsets.only(top: 15),
              padding: EdgeInsets.all(5),
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('icon-success.png'))),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 25, vertical: 15),
              child: Column(children: [
                Text('Permintaan Diproses', style: TextStyle(height: 3, color: mainColor, fontSize: 22)),
                Text(
                    'Permintaan Anda sudah kami terima. Kami akan menghubungi Anda',
                    style: TextStyle(height: 2), textAlign: TextAlign.center,),
              ]),
            ),
            buttonOutlineBlue('Kembali ke Halaman Utama', MainPage())
          ],
        ));
  }
}
