part of 'pages.dart';

class LayananPage extends StatefulWidget {
  @override
  _LayananPageState createState() => _LayananPageState();
}

class _LayananPageState extends State<LayananPage> {
  @override
  Widget build(BuildContext context) {
    return GeneralPage(
        title: 'Layanan',
        onBackButtonPressed: () {
          Navigator.pop(context);
        },
        child: Column(
          children: [
            cardLayanan('kesehatan.png', 'Kesehatan \nLayanan', '3498DB', LayananKesehatan()),
            cardLayanan('homecare.png', 'Perawatan \ndi Rumah', '2ECC71', LayananHomeCare()),
            cardLayanan('bansos.png', 'Pendaftaran \nBansos', 'F39C12', RegisterBansos()),
          ],
        ));
  }

  InkWell cardLayanan(String image, String text, String color, page) {
    return InkWell(
      child: Container(
        margin: EdgeInsets.all(15),
        padding: EdgeInsets.all(25),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(25),
          color: color.toColor(),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Image(image: AssetImage(image)),
            Text(text, style: TextStyle(color: Colors.white, fontSize: 21)),
            Icon(Icons.chevron_right, size: 45, color: Colors.white)
          ],
        ),
      ),
      onTap: () {
        Get.to(page);
      },
    );
  }
}
