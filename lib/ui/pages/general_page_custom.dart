part of 'pages.dart';

class GeneralPageCustom extends StatelessWidget {
  final String title;
  final String alamat;
  final String date;
  final String nik;
  final Function? onBackButtonPressed;
  final Function? directButton;
  final Widget? child;
  final Color? backColor;

  const GeneralPageCustom(
      {this.title = 'Title',
      this.alamat = 'Subtitle',
      this.date = '8 Juni 1965',
      this.nik = '1234-5678-91234556',
      this.directButton,
      this.onBackButtonPressed,
      this.child,
      this.backColor});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: [
        SafeArea(
          child: ListView(children: [
            Column(children: [
              Container(
                // margin: EdgeInsets.only(bottom: defaultMargin),
                padding: EdgeInsets.symmetric(horizontal: 15),
                width: double.infinity,
                height: 150,
                color: secondaryColor,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    onBackButtonPressed != null
                        ? GestureDetector(
                            onTap: () {
                              if (onBackButtonPressed != null) {
                                onBackButtonPressed!();
                              }
                            },
                            child: Container(
                              width: 24,
                              height: 24,
                              margin: EdgeInsets.only(right: 15, bottom: 20),
                              child:
                                  Icon(Icons.arrow_back, color: Colors.white),
                            ),
                          )
                        : SizedBox(),
                    Row(
                      children: [
                        Container(
                          width: 50,
                          height: 50,
                          margin: EdgeInsets.only(right: 10),
                          padding: EdgeInsets.all(5),
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  image: AssetImage('avatar.png'),
                                  fit: BoxFit.cover)),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              title,
                              style: GoogleFonts.poppins(
                                  fontSize: 14,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.white),
                            ),
                            Container(
                              child: Row(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(right: 10),
                                    child: Text(
                                      date,
                                      style: GoogleFonts.poppins(
                                          fontSize: 11, color: Colors.white),
                                    ),
                                  ),
                                  Text(
                                    nik,
                                    style: GoogleFonts.poppins(
                                        fontSize: 11, color: Colors.white),
                                  ),
                                  directButton != null
                                      ? GestureDetector(
                                          child: Icon(Icons.play_circle_filled,
                                              color: Colors.white),
                                          onTap: () {
                                            if (directButton != null) {
                                              directButton!();
                                            }
                                          },
                                        )
                                      : SizedBox(),
                                ],
                              ),
                            ),
                            Text(
                              alamat,
                              style: GoogleFonts.poppins(
                                  fontSize: 11, color: Colors.white),
                            )
                          ],
                        )
                      ],
                    )
                  ],
                ),
              ),
              child ?? SizedBox()
            ]),
          ]),
        ),
      ]),
    );
  }
}
