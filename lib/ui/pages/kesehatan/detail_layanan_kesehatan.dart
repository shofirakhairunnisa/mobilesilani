part of '../pages.dart';

class DetailLayanan extends StatefulWidget {
  @override
  _DetailLayananState createState() => _DetailLayananState();
}

class _DetailLayananState extends State<DetailLayanan> {
  @override
  Widget build(BuildContext context) {
    return GeneralPage(
        title: 'Layanan Kesehatan',
        onBackButtonPressed: () {
          Navigator.pop(context);
        },
        child: Column(
          children: [
            Container(
              width: double.infinity,
              color: Colors.black12,
              padding: EdgeInsets.all(20),
              child: Text(
                'Pilih Fasilitas Kesehatan',
                style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
              ),
            ),
          ],
        ));
  }
}
