part of '../pages.dart';

class LayananKesehatan extends StatefulWidget {
  @override
  _LayananKesehatanState createState() => _LayananKesehatanState();
}

class _LayananKesehatanState extends State<LayananKesehatan> {
  @override
  Widget build(BuildContext context) {
    return GeneralPage(
        title: 'Layanan Kesehatan',
        onBackButtonPressed: () {
          Navigator.pop(context);
        },
        child: Column(
          children: [
            Container(
              width: double.infinity,
              color: Colors.black12,
              padding: EdgeInsets.all(20),
              child: Text(
                'Pilih Fasilitas Kesehatan',
                style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
              ),
            ),
            listLayanan('Puskesmas Pembantu Gowasari I', 'Gowasari',
                'Ditanggung oleh KIS Anda'),
            listLayanan(
                'Rumah Sakit Senopati', 'Senopat', 'Ditanggung oleh KIS Anda'),
            listLayanan('Puskesmas Pajangan', 'Jln Benyo Pajangan', ''),
          ],
        ));
  }
}
