part of '../pages.dart';

class UpdatePassword extends StatefulWidget {
  const UpdatePassword({Key? key}) : super(key: key);

  @override
  _UpdatePasswordState createState() => _UpdatePasswordState();
}

class _UpdatePasswordState extends State<UpdatePassword> {
  TextEditingController oldPasswordController = TextEditingController();
  TextEditingController newPasswordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return GeneralPage(
      title: 'Update Profil',
      onBackButtonPressed: () {
        Navigator.pop(context);
      },
      child: Column(
        children: [
          inputField(oldPasswordController, true, 'Password Lama'),
          inputField(newPasswordController, true, 'Password Baru'),
          inputField(confirmPasswordController, true, 'Konfirmasi Password'),
          button('Perbarui', double.infinity, SignInPage())
        ],
      ),
    );
  }
}
