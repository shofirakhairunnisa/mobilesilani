part of '../pages.dart';

class DetailProfile extends StatefulWidget {
  @override
  _DetailProfileState createState() => _DetailProfileState();
}

class _DetailProfileState extends State<DetailProfile> {
  @override
  Widget build(BuildContext context) {
    return GeneralPageCustom(
        title: 'Rusty Shackleford',
        alamat: 'Dusun Shackleford',
        date: '10 Juni 1951',
        nik: 'ID-1234-5678-91234556',
        onBackButtonPressed: () {
          Navigator.pop(context);
        },
        child: Column(
          children: [
            Container(
              width: double.infinity,
              color: Colors.black12,
              padding: EdgeInsets.all(20),
              child: Text(
                'Detail Layanan',
                style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
              ),
            ),
            listDetail('20 Januari 2020 08:00', 'Cek kesehatan di Puskesmas',
                true),listDetail('20 Januari 2021 08:00', 'Cek Mata di Puskesmas',
                false),
          ],
        ));
  }
}
