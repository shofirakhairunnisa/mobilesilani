part of '../pages.dart';

class UpdateProfile extends StatefulWidget {
  const UpdateProfile({Key? key}) : super(key: key);

  @override
  _UpdateProfileState createState() => _UpdateProfileState();
}

class _UpdateProfileState extends State<UpdateProfile> {
  TextEditingController nikController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController tempatLahirController = TextEditingController();
  TextEditingController tanggalLahirController = TextEditingController();
  TextEditingController alamatController = TextEditingController();
  TextEditingController noTeleponController = TextEditingController();
  TextEditingController noBpjsController = TextEditingController();
  TextEditingController emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return GeneralPage(
      title: 'Update Profil',
      onBackButtonPressed: () {
        Navigator.pop(context);
      },
      child: Column(
        children: [
          inputField(nikController, false, 'NIK'),
          inputField(nameController, false, 'Nama Lengkap'),
          inputField(tempatLahirController, false, 'Tempat Lahir'),
          inputField(tanggalLahirController, false, 'Tanggal Lahir'),
          inputField(alamatController, false, 'Alamat'),
          inputField(noTeleponController, false, 'No Telepon'),
          inputField(noBpjsController, false, 'No Bpjs'),
          inputField(emailController, false, 'Email'),
          button('Perbarui', double.infinity, ProfilePage())
        ],
      ),
    );
  }
}
