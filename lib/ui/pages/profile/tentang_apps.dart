part of '../pages.dart';

class TentangApps extends StatefulWidget {
  @override
  _TentangAppsState createState() => _TentangAppsState();
}

class _TentangAppsState extends State<TentangApps> {
  @override
  Widget build(BuildContext context) {
    return GeneralPage(
        title: 'Tentang Aplikasi',
        onBackButtonPressed: () {
          Navigator.pop(context);
        },
        child: Column(
          children: [
            Container(
              width: 240,
              height: 200,
              margin: EdgeInsets.only(top: 15),
              padding: EdgeInsets.all(5),
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('../../../assets/splash-image.png'), fit: BoxFit.fill)),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 25, vertical: 15),
              child: Text(
                  'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry`s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
              style: TextStyle(height: 2),),
            )
          ],
        ));
  }
}
