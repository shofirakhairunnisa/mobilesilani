part of 'pages.dart';

class TipsPage extends StatefulWidget {

  @override
  _TipsPageState createState() => _TipsPageState();
}

class _TipsPageState extends State<TipsPage> {

  @override
  Widget build(BuildContext context) {
    return GeneralPage(
        title: 'Kategori & Tips',
        onBackButtonPressed: () {
          Navigator.pop(context);
        },
        child: Column(
          children: [
            InkWell(
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 15),
                child: Text(
                  'Kegiatan di Lingkungan',
                  style:
                  TextStyle(fontWeight: FontWeight.w500, fontSize: 16)
                ),
              ),
              onTap: () {},
            ),InkWell(
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 15),
                child: Text(
                  'Tips untuk Anda',
                  style:
                  TextStyle(fontWeight: FontWeight.w500, fontSize: 16)
                ),
              ),
              onTap: () {},
            ),
          ],
        )
    );
  }
}
