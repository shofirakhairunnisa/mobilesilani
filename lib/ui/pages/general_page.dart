part of 'pages.dart';

class GeneralPage extends StatelessWidget {
  final String title;
  final Function? onBackButtonPressed;
  final Widget? child;
  final Color? backColor;

  const GeneralPage(
      {this.title = 'Title',
        this.onBackButtonPressed,
        this.child,
        this.backColor});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: [
        SafeArea(
          child: ListView(children: [
            Column(children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: defaultMargin),
                width: double.infinity,
                height: 70,
                color: secondaryColor,
                child: Row(
                  children: [
                    onBackButtonPressed != null
                        ? GestureDetector(
                      onTap: () {
                        if (onBackButtonPressed != null) {
                          onBackButtonPressed!();
                        }
                      },
                      child: Container(
                        width: 24,
                        height: 24,
                        margin: EdgeInsets.only(right: 26),
                        child: Icon(Icons.arrow_back, color: Colors.white),
                      ),
                    )
                        : SizedBox(),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          title,
                          style: GoogleFonts.poppins(
                              fontSize: 20, fontWeight: FontWeight.w500, color: Colors.white),
                        )
                      ],
                    )
                  ],
                ),
              ),
              child ?? SizedBox()
            ]),
          ]),
        ),
      ]),
    );
  }
}
