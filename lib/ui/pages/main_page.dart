// part of 'pages.dart';
//
// class MainPage extends StatefulWidget {
//   const MainPage({Key? key}) : super(key: key);
//
//   @override
//   _MainPageState createState() => _MainPageState();
// }
//
// class _MainPageState extends State<MainPage> {
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       child: ElevatedButton(
//         onPressed: () {
//           Navigator.pushReplacement(context,
//               MaterialPageRoute(builder: (context) {
//             return SignInPage();
//           }));
//         },
//         style: ElevatedButton.styleFrom(
//             shape:
//                 RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
//             primary: greyColor),
//         child: Text(
//           'Logout',
//           style: GoogleFonts.poppins(
//               color: Colors.white, fontWeight: FontWeight.w500),
//         ),
//       ),
//     );
//   }
// }
part of 'pages.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int selectedPage = 0;
  PageController pageController = PageController(initialPage: 0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            color: Colors.white,
          ),
          SafeArea(
            child: Container(color: 'FAFAFC'.toColor()),
          ),
          SafeArea(
            child: PageView(
                controller: pageController,
                onPageChanged: (index) {
                  setState(() {
                    selectedPage = index;
                  });
                },
                children: [
                  Center(child: HomePage()),
                  Center(child: TipsPage()),
                  Center(child: LayananPage()),
                  Center(child: ProfilePage()),
                ]),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: CustomBottomNavbar(
              selectedIndex: selectedPage,
              onTap: (index) {
                setState(() {
                  selectedPage = index;
                });
                pageController.jumpTo(selectedPage.toDouble());
              },
            ),
          )
        ],
      ),
    );
  }
}

