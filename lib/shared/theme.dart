part of 'shared.dart';

Color mainColor = 'EE6F57'.toColor();
Color secondaryColor = '14274E'.toColor();
Color greyColor = 'BDBDBD'.toColor();
Color greyColor2 = Colors.black45;

TextStyle greyFontStyle = GoogleFonts.poppins().copyWith(color: greyColor);
TextStyle blackFontStyle1 = GoogleFonts.poppins()
    .copyWith(color: Colors.black, fontSize: 22, fontWeight: FontWeight.w500);
TextStyle blackFontStyle2 = GoogleFonts.poppins()
    .copyWith(color: Colors.black, fontSize: 16, fontWeight: FontWeight.w500);
TextStyle blackFontStyle3 = GoogleFonts.poppins()
    .copyWith(color: Colors.black,);

const double defaultMargin = 24;
